﻿using System;
using System.Threading;

namespace PrimersThreads
{
    class Program
    {

        private static int lines;
        static void Main()
        {

            OneOnlyThread();

        }
        static void OneOnlyThread()
        {
            /***** Pas de paràmetre en un thread***/
            Thread FirstFile = new Thread(funcThreadParam);
            FirstFile.Start("input1.txt");
            Thread.Sleep(50);// no se bien pero con el sleep funciona
            Thread SecondFile = new Thread(funcThreadParam);
            SecondFile.Start("input2.txt");
            Thread ThirdFile = new Thread(funcThreadParam);
            ThirdFile.Start("input3.txt");
            Thread FourthFile = new Thread(funcThreadParam);
            FourthFile.Start("input4.txt");
        }


        static void funcThreadParam(object fichero)
        {
            string textFile = @"..\..\..\inputs\" + fichero;
            lines = GetNumberOfLinesOfADocument(textFile);
            var prova = new Prova();

            prova.RunProva(lines);
        }
        private static int GetNumberOfLinesOfADocument(string textFile)
        {

            string[] lines = System.IO.File.ReadAllLines(textFile);

            return lines.Length;
        }
    }
}
