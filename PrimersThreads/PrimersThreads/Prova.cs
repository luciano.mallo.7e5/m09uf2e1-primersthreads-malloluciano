﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace PrimersThreads
{
    class Prova
    {

        private bool _outBool = false;
        public void RunProva(int lines)
        {
            FindNumberThread(lines);
            // FindNumbreOfLines(lines);
        }

        public void FindNumbreOfLines(object lines)
        {
            string linesStrg = Convert.ToString(lines);
            Random rnd = new Random();
            int randomInt;
            int positionOfNumOfLine = 0;
            do
            {
                randomInt = rnd.Next(0, 10);
                if (randomInt == linesStrg[positionOfNumOfLine] && positionOfNumOfLine < linesStrg.Length)
                {
                    positionOfNumOfLine++;

                }
                else
                {
                    _outBool = true;
                }

            } while (_outBool != true);
            Console.WriteLine("The files has: {0}", lines);
        }

        public void FindNumberThread(int lines)
        {

            Thread ThreadWithParam = new Thread(FindNumbreOfLines);
            ThreadWithParam.Start(lines);

        }
    }
}
