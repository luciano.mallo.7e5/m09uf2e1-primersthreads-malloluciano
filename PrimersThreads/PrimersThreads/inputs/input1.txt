El tres porquets

Al cor del bosc hi vivien tres porquets que eren germans.
El llop sempre els perseguia per menjar-se'ls.
Per poder escapar del llop, els tres porquets van decidir fer-se una casa.

El més petit va fer-se una casa de palla, per anar més de pressa i poder anar-se'n a jugar.
El mitjà es va construir una casa de fusta. En veure que el seu germà petit ja havia acabat, s'afanyà per acabar rapidament per poder anar-se'n a jugar amb ell.
I el més gran treballava fort en la construcción de la seva casa feta amb totxos.
- Ja veureu el que farà el llop amb les vostres cases - va renyar als seus germans mentre aquests s'ho passaven la mar de bé jugant sense cap preocupació.

Un dia el llop va sortir de sobte de darrera un arbre i va saltar darrera el porquet petit i el perseguí. El porquet va córrer i córrer fins a la seva caseta de palla, el llop s'aturà i va començar a bufar i bufar fins que la caseta de palla va caure.

El llop llavors va perseguir al porquet petit pel bosc que va córrer per a poder refugiar-se a casa del seu germà mitjà. Però en arribar-hi el llop va tornar a bufar i bufar fins que la caseta de fusta també va caure.
Els dos porquets van sortir corrent i perseguits per l'afamat llop .

Gairebé sense alè i amb el llop enganxat a les sabates, van arribar a casa del germà gran.
Tots tres van entrar-hi rapidament i van tancar bé totes les portes i finestres.
El llop bufà i bufà, però la casa no queia. Bufà i bufà, però la casa aguantava. Cansat i sense alè el llop va començar a donar voltes a la casa, buscant algun lloc per on poder entrar. Amb una escala llarguíssima va pujar a la teulada, per colar-se per la xemeneia. Però el porquet gran va posar al foc una olla amb aigua.
El llop golafre va baixar per l'interior de la xemeneia, però va caure dins de l'olla amb l'aigua bullint i es va escaldar.
Va fer un salt i va fugir d'allà amb uns terribles udols que es van sentir per tot el bosc, i diuen que des d'aquell dia mai més va voler menjar porquet.

Fi